**Run below command(s) to set up the environment first**
```
sudo apt install -y docker.io
sudo apt install -y docker-compose
```

Run below command(s) to get to the directory

```
cd NextGen-track-oneAnother
```
Run below command(s) to start the application

#in attached mode
```
sudo docker-compose up
```

#in detached mode
```
sudo docker-compose up -d
```

To get to NextGen Auth Service

http://server-IP:3001/api

To get to NextGen Location Service

http://server-IP:3002

To get to pgAdmin Dahboard

http://server-IP:3007

> email: admin@trackoneanother.com

> password: admin123

Database Information to use:

> Maintenance Database: _**company_db**_
> Username: _**trackoneanother**_
> Password: _**1Bismilla**_
